import React, {useState, useEffect} from 'react';
import {Container, Table} from 'react-bootstrap';


export default function Leaderboard() {

    const [players, setPlayers] = useState([])

    // fetch all data from the database and save it to players variable
    useEffect(() => {
        fetch('http://localhost:4000/api/scores')
            .then(res => res.json())
            .then(data => {
                setPlayers(data)
            })        
    }, [])

    return (
        <Container>
            <h1>Tic-Tac-Toe</h1>
            <Table className="mx-5">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Score</th>
                    </tr>
                </thead>
                <tbody>
                    {players.map(player => {
                        return (
                            <tr key={player._id}>
                                <td>{player.name}</td>
                                <td>{player.score}</td>
                            </tr>
                        )
                    })}

                </tbody>
            </Table>
        </Container>
    )
}
