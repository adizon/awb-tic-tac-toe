import React, {useState, useEffect} from 'react';
import {Container, Button, Row, Col, Badge} from 'react-bootstrap';
import {useHistory} from 'react-router-dom';
import Swal from 'sweetalert2'

function PlayGame() {

    let history=useHistory()

    // retrieve player names
    let player1 = localStorage.getItem('P1Name')
    let player2 = localStorage.getItem('P2Name')

    // indicator/flag of whose turn is it
    const [turns, setTurns] = useState(false)

    // cells of the tic-tac-toe board
    const [text1, setText1] = useState("-")
    const [text2, setText2] = useState("-")
    const [text3, setText3] = useState("-")
    const [text4, setText4] = useState("-")
    const [text5, setText5] = useState("-")
    const [text6, setText6] = useState("-")
    const [text7, setText7] = useState("-")
    const [text8, setText8] = useState("-")
    const [text9, setText9] = useState("-")

    useEffect(() => {
        if (text1 === "-" || text2 === "-" || text3 === "-" ||
            text4 === "-" || text5 === "-" || text6 === "-" ||
            text7 === "-" || text8 === "-" || text9 === "-") {
            
            // checking if Player 1 wins
            if (analyzeTurn("X") === 1) {
                Swal.fire(`${player1} won!`, '', 'success')

                // update the database.
                fetch('http://localhost:4000/api/scores', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        name: player1,
                        score: 1
                    })
                })
                .then (res => res.json())
                .then (data => {
                    if (data === true) {
                        localStorage.clear();
                        history.push('/')
                    }
                })
            } 

            // checking if Player 2 wins
            if (analyzeTurn("O") === 1) {
                Swal.fire(`${player2} won!`, '', 'success')

                // update the database
                fetch('http://localhost:4000/api/scores', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        name: player2,
                        score: 1
                    })
                })
                .then (res => res.json())
                .then (data => {
                    if (data === true) {
                        localStorage.clear();
                        history.push('/')
                    }
                })
            }
        } else {
            Swal.fire(`It's a draw!`, '', 'error')
            localStorage.clear();
            history.push('/')
        }

    }, [text1, text2, text3, text4, text5, text6, text7,text8, text9])

    // this function will check the board for possible winner
    function analyzeTurn(text) {
        if (text === text1 && text === text2 && text === text3) {
            return 1
        } else if (text === text4 && text === text5 && text === text6) {
            return 1
        } else if (text === text7 && text === text8 && text === text9) {
            return 1
        } else if (text === text1 && text === text4 && text === text7) {
            return 1
        } else if (text === text2 && text === text5 && text === text8) {
            return 1
        } else if (text === text3 && text === text6 && text === text9) {
            return 1
        } else if (text === text1 && text === text5 && text === text9) {
            return 1
        } else if (text === text3 && text === text5 && text === text7) {
            return 1
        }
    }

    // these functions will flip the turns variable to indicate whose turn is it,
    // and then place the appropriate marking in the board (X or O)
    function onClickHandler1() {
        if (text1 === "-") {
            if (turns) {
                setText1("O")
            } else {
                setText1("X")
            }
            setTurns(!turns)
        }

    }

    function onClickHandler2() {
        if (text2 === "-") {
            if (turns) {
                setText2("O")
            } else {
                setText2("X")
            }
            setTurns(!turns)
        }
    }

    function onClickHandler3() {
        if (text3 === "-") {
            if (turns) {
                setText3("O")
            } else {
                setText3("X")
            }
            setTurns(!turns)
        }
    }

    function onClickHandler4() {
        if (text4 === "-") {
            if (turns) {
                setText4("O")
            } else {
                setText4("X")
            }
            setTurns(!turns)
        }
    }

    function onClickHandler5() {
        if (text5 === "-") {
        
            if (turns) {
                setText5("O")
            } else {
                setText5("X")
            }
            setTurns(!turns)
        }
    }

    function onClickHandler6() {
        if (text6 === "-") {
            if (turns) {
                setText6("O")
            } else {
                setText6("X")
            }
            setTurns(!turns)
        }
    }

    function onClickHandler7() {
        if (text7 === "-") {
            if (turns) {
                setText7("O")
            } else {
                setText7("X")
            }

            setTurns(!turns)
        }
    }

    function onClickHandler8() {
        if (text8 === "-") {
            if (turns) {
                setText8("O")
            } else {
                setText8("X")
            }

            setTurns(!turns)
        }
    }

    function onClickHandler9() {
        if (text9 === "-") {
            if (turns) {
                setText9("O")
            } else {
                setText9("X")
            }
            setTurns(!turns)
        }
    }

    return (
        <Container>
        <h1>Tic-Tac-Toe</h1>
        <p>{player1}
        {
        !turns
        ? <Badge pill variant="success">-</Badge>
        : null 
        }
        </p>
        <p>{player2}
        {
        turns
        ? <Badge pill variant="success">-</Badge>
        : null 
        }
        </p>
        <br/>
        <Row className="mx-5">
            <Col xs={12} lg={4}>
                <Row className="mx-5">
                    <Col>
                        <Button onClick={()=>onClickHandler1()} variant="light">{text1}</Button>
                    </Col>
                    <Col>
                        <Button onClick={()=>onClickHandler2()} variant="light">{text2}</Button>
                    </Col>
                    <Col>
                        <Button onClick={()=>onClickHandler3()} variant="light">{text3}</Button>
                    </Col>
                </Row>
                <br/>
                <Row className="mx-5">
                    <Col>
                        <Button onClick={()=>onClickHandler4()} variant="light">{text4}</Button>
                    </Col>
                    <Col>
                        <Button onClick={()=>onClickHandler5()} variant="light">{text5}</Button>
                    </Col>
                    <Col>
                        <Button onClick={()=>onClickHandler6()} variant="light">{text6}</Button>
                    </Col>
                </Row>
                <br/>
                <Row className="mx-5">
                    <Col>
                        <Button onClick={()=>onClickHandler7()} variant="light">{text7}</Button>
                    </Col>
                    <Col>
                        <Button onClick={()=>onClickHandler8()} variant="light">{text8}</Button>
                    </Col>
                    <Col>
                        <Button onClick={()=>onClickHandler9()} variant="light">{text9}</Button>
                    </Col>
                </Row>
            </Col>
        </Row>
        </Container>
    );
}

export default PlayGame