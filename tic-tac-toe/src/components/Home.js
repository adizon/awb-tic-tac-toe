import React, {useState} from 'react';
import {Container, Button, Row, Form} from 'react-bootstrap';
import {useHistory} from 'react-router-dom';

function Home() {
    const [playGame, setPlayGame] = useState(false)
    const [player1, setPlayer1] = useState("")
    const [player2, setPlayer2] = useState("")
    let history=useHistory()

    function onClickHandler(){
        // save the players' name to localStorage
        localStorage.setItem('P1Name', player1)
        localStorage.setItem('P2Name', player2)

        // call the PlayGame component
        history.push('/play')
    }

    return (
        <Container>
        <h1>Tic-Tac-Toe</h1>
        <br/>
        <Row>
            <Button variant="danger" onClick={()=>setPlayGame(!playGame)}>Play Game</Button>
        </Row>
        <Row>
            {
                // the form will only show up when the Play Game button is clicked
                playGame
                ? <Form>
                    <Form.Group>
                        Player 1:
                        <Form.Control value={player1} onChange={(e)=>setPlayer1(e.target.value)} type="text" name="player1"/>
                    </Form.Group>
                    <Form.Group>
                        Player 2:
                        <Form.Control value={player2} onChange={(e)=>setPlayer2(e.target.value)} type="text" name="player2"/>
                    </Form.Group>
                    <Form.Group>
                        <Button  onClick={()=>onClickHandler()} variant="success">Submit</Button>
                    </Form.Group>                    
                </Form>
                : null
            }
        </Row>
        <br/>
        <Row>
            {
                // call the Leaderboard component
            }
            <Button variant="info" onClick={()=>history.push('/ranking')}>View Leaderboard</Button>
        </Row>
        </Container>
    );
}

export default Home