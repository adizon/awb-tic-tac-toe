import 'bootstrap/dist/css/bootstrap.min.css'
import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import Home from './components/Home';
import PlayGame from './components/PlayGame'
import Leaderboard from './components/Leaderboard'

ReactDOM.render(
  <React.StrictMode>
	<BrowserRouter>
		<Switch>
			<Route exact path="/" component={Home}/>
			<Route exact path="/play" component={PlayGame}/>
			<Route exact path="/ranking" component={Leaderboard}/>
		</Switch>
	</BrowserRouter>    
  </React.StrictMode>,
  document.getElementById('root')
);
